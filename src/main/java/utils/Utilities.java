package utils;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Properties;

/**
 * This class contains methods which are usually
 * placed in frameworks. Methods here do
 * functionalities which are used frequently
 * and are not directly related to a specific web page
 * this avoids code duplication and reduces bugs.
 */

public class Utilities {

    /**
     * Method which returns the content of the href
     * attribute of a particular element.
     *
     * @param driver current web driver.
     * @param element element containing the href attribute.
     * @return the URL found in the href attribute.
     */

    public static String getHrefAttributeContent(WebDriver driver, By element){
        return driver.findElement(element).getAttribute("href");
    }

    /**
     *
     * @param retrievedUrl string returned by the getHrefAttributeContent method.
     * @return returns a response code.
     * @throws IOException
     */

    public static Integer getResponseForUrl(String retrievedUrl) throws IOException {
        int response = 0;

        try {
            URL url = new URL(retrievedUrl);
            HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();

            httpCon.setRequestMethod("GET");
            httpCon.connect();

            response = httpCon.getResponseCode();


        } catch (NullPointerException e) {
            System.out.println("URL is null.");
        }

        return response;
    }

    /**
     * Method which scrolls down until the
     * element comes into the view port.
     *
     * @param driver Current web driver.
     * @param element The element being searched for.
     */

    public static void scrollToElement(WebDriver driver,By element){
        driver.findElement(element);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);

    }


    /**
     * This method creates an InputStream instance
     * with the given property file directory and
     * returns the value of the property specified.
     *
     * @param value The name of the property to be retrieved.
     * @return returns the value of the property as String.
     */

    public static String getPropertyValue(String value){
        String propertyValue = null;

        try (InputStream input = new FileInputStream(System.getProperty("propertyFilePath"))) {
            Properties prop = new Properties();
            prop.load(input);

            propertyValue = prop.getProperty(value);


        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return propertyValue;
    }

    /**
     * Method which acts as a thread sleep but dependent
     * on a particular element.
     *
     * @param driver Current web driver.
     * @param timeout Desired timeout.
     * @param element Particular element to wait for.
     */

    public static void waitForElement(WebDriver driver, int timeout, By element){
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    /**
     * Method which finds a particular element
     * and returns the text attribute.
     * @param driver Current web driver.
     * @param element Particular element.
     * @return String containing the value in the text attribute.
     */

    public static String getElementText(WebDriver driver, By element){
        //waitForElement(driver, 10, element);
        return driver.findElement(element).getText();
    }


}
