package utils;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class Driver {

    private static WebDriver driver;

    /**
     * Web driver instantiation method
     * @param webDriver this parameter is being propogated
     *                  from the vm options, this allows the
     *                  user to switch to the desired browser
     *                  without the need of code alteration.
     */

    public static void startWebDriver(String webDriver){
        if(webDriver == null){
            throw new IllegalStateException("WebDriver not specified in program variables");
        }

        /* The switch case initiates a web driver depending on
        the value inputted in the vm option. In case of null/invalid
        input an exception is thrown. */

        switch (webDriver) {
            case "chrome": WebDriverManager.chromedriver().setup();
            driver = new ChromeDriver();
            break;

            case "firefox": WebDriverManager.firefoxdriver().setup();
            driver = new FirefoxDriver();
            break;

            case "ie": WebDriverManager.iedriver().setup();
            driver = new InternetExplorerDriver();
            break;

            case "edge": WebDriverManager.edgedriver().setup();
            driver = new EdgeDriver();
            break;

            default: throw new IllegalArgumentException("WebDriver specified not recognised, " +
                    "please use one of the following: chrome, firefox, ie, edge");
        }

        driver.manage().window().maximize();

    }

    /**
     * Method which returns the driver object to be used
     * at page object level.
     *
     * @return throws an exception if web driver is null.
     */
    public static WebDriver getWebDriver(){
        if(driver == null){
            throw new IllegalStateException("Web driver not initialized");
        }

        return driver;
    }

    /**
     * This method is responsible of instructing
     * the driver to browse to the desired URL.
     * This is also found in the vm options.
     *
     * @param url This parameter stores the value
     *            of the relevant vm option.
     */

    public static void navigateTo(String url){
        driver.navigate().to(url);
    }

    /**
     * Closes web driver.
     */

    public static void stopWebDriver(){
        if(driver != null){
            driver.close();
        }

    }


}
