package main;

import assertmanagers.LandingPageAssert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pagemanagers.LandingPageManager;
import utils.Driver;

import java.io.IOException;

@Test()
public class CorrectPageTest {

    @BeforeTest
    public void initiateDriverAndSetupPage(){
        Driver.startWebDriver(System.getProperty("browser"));

    }

    @Test(testName = "Real Money URL Test")
    public void realMoneyURLTest() throws IOException {
        LandingPageManager.browseToSubdirectory("realMoney");
        LandingPageAssert.verifyRealMoneySubdirectory("Real Money");

    }

    @Test(testName = "Live Casino URL Test")
    public void liveCasinoURLTest() throws IOException {
        LandingPageManager.browseToSubdirectory("liveCasino");
        LandingPageAssert.verifyLiveCasinoSubdirectory("Live Dealer Casinos");

    }

    @Test(testName = "Payment methods URL Test")
    public void paymentMethodsURLTest() throws IOException {
        LandingPageManager.browseToSubdirectory("paymentMethods");
        LandingPageAssert.verifyPaymentMethodsSubdirectory("Deposit Methods");

    }

    @Test(testName = "Table Games URL Test")
    public void tableGamesURLTest() throws IOException {
        LandingPageManager.browseToSubdirectory("tableGames");
        LandingPageAssert.verifyTableGamesSubdirectory("Table Games");

    }

    @Test(testName = "Casino Reviews URL Test")
    public void reviewsURLTest() throws IOException {
        LandingPageManager.browseToSubdirectory("topQualityCasino");
        LandingPageAssert.verifyTopCasinosSubdirectory("Casino Reviews");

    }

    @Test(testName = "No Deposit URL Test")
    public void noDepositURLTest() throws IOException {
        LandingPageManager.browseToSubdirectory("noDepositBonuses");
        LandingPageAssert.verifyNoDepositSubdirectory("No Deposit Bonus");

    }

    @AfterTest
    public void stopWebDriver(){
        Driver.stopWebDriver();
    }



}
