package pagemanagers;

import pageobjects.LandingPage;
import utils.Driver;

import static utils.Utilities.getPropertyValue;


/** This class is responsible of
 *  joining all the actions specified
 *  at page object level to create an
 *  end-to-end journey which then results
 *  in a test case.
 */

public class LandingPageManager {

    /**
     * Method that allows the programme to navigate
     * to different subdirectories by getting the property
     * value from a properties file.
     *
     * @param subdirectory String variable which contains
     *                     subdirectory details passed from
     *                     test case level.
     */

    public static void browseToSubdirectory(String subdirectory){
        Driver.navigateTo(System.getProperty("baseURL") + getPropertyValue(subdirectory));
    }

}
