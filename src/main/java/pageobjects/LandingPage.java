package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utils.Driver;

import static utils.Utilities.getElementText;

/** This class is contains the elements
 *  and other page object found on a specific
 *  web page. Each class within this package
 *  represents a page.
 *
 *  This class also contains actions related
 *  to objects like click and sendKeys.
 */

public class LandingPage {

    private WebDriver driver = Driver.getWebDriver();

    private By realMoneyTitle  = By.className("main-title");
    private By liveCasinoTitle = By.className("main-title");
    private By payMethodsTitle = By.xpath("//*[@id=\"vegas-body\"]/main/div[2]/div[3]/h1");
    private By tableGamesTitle = By.className("main-title");
    private By topCasinosTitle = By.xpath("//*[@id=\"vegas-body\"]/main/div[5]/section[1]/div/div/div[1]/h1");
    private By noDepositTitle  = By.className("main-title");


    public String getRealMoneyTitleText(){
        return getElementText(driver, realMoneyTitle);
    }

    public String getLiveCasinoTitleText(){
        return getElementText(driver, liveCasinoTitle);
    }

    public String getPaymentMethodsTitleText(){
        return getElementText(driver, payMethodsTitle);
    }

    public String getTableGamesTitleText(){
        return getElementText(driver, tableGamesTitle);
    }

    public String getTopCasinosTitleText(){
        return getElementText(driver, topCasinosTitle);
    }

    public String getNoDepositTitleText(){
        return getElementText(driver, noDepositTitle);
    }

}
