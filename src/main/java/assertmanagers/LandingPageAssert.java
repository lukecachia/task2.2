package assertmanagers;

import org.testng.Assert;
import pageobjects.LandingPage;

import java.io.IOException;

import static utils.Utilities.getPropertyValue;
import static utils.Utilities.getResponseForUrl;

/** This class contains methods which assert
 *  required verifications and returns a result
 *  according to the expected result.
 */

public class LandingPageAssert {

    private static LandingPage landingPage = new LandingPage();

    /**
     * This method uses the expectedContent argument passed
     * at test case level to assert it matches with the
     * data provided from a page object level.
     *
     * @param expectedContent String parameter which contains
     *                        expected data on the web page.
     */

    public static void verifyRealMoneySubdirectory(String expectedContent) throws IOException {
        if(getResponseForUrl(System.getProperty("baseURL") + getPropertyValue("realMoney")) != 404) {
            if(!landingPage.getRealMoneyTitleText().contains(expectedContent)){
                Assert.fail("The URL browsed might not be a correct one");
            }
        } else {
            Assert.fail("Subdirectory not found! Check subdirectory provided in the properties file.");
        }

    }

    public static void verifyLiveCasinoSubdirectory(String expectedContent) throws IOException {
        if(getResponseForUrl(System.getProperty("baseURL") + getPropertyValue("liveCasino")) != 404){
            if(!landingPage.getLiveCasinoTitleText().contains(expectedContent)){
                Assert.fail("The URL browsed might not be a correct one");
            }
        } else {
            Assert.fail("Subdirectory not found! Check subdirectory provided in the properties file.");
        }

    }

    public static void verifyPaymentMethodsSubdirectory(String expectedContent) throws IOException {
        if(getResponseForUrl(System.getProperty("baseURL") + getPropertyValue("paymentMethods")) != 404) {
            if (!landingPage.getPaymentMethodsTitleText().contains(expectedContent)) {
                Assert.fail("The URL browsed might not be a correct one");
            }
        } else {
            Assert.fail("Subdirectory not found! Check subdirectory provided in the properties file.");
        }
    }

    public static void verifyTableGamesSubdirectory(String expectedContent) throws IOException {
        if(getResponseForUrl(System.getProperty("baseURL") + getPropertyValue("tableGames")) != 404) {
            if(!landingPage.getTableGamesTitleText().contains(expectedContent)){
                Assert.fail("The URL browsed might not be a correct one");
            }
        } else {
            Assert.fail("Subdirectory not found! Check subdirectory provided in the properties file.");
        }
    }

    public static void verifyTopCasinosSubdirectory(String expectedContent) throws IOException {
        if(getResponseForUrl(System.getProperty("baseURL") + getPropertyValue("topQualityCasino")) != 404) {
            if(!landingPage.getTopCasinosTitleText().contains(expectedContent)){
                Assert.fail("The URL browsed might not be a correct one");
            }
        } else {
            Assert.fail("Subdirectory not found! Check subdirectory provided in the properties file.");
        }
    }

    public static void verifyNoDepositSubdirectory(String expectedContent) throws IOException {
        if(getResponseForUrl(System.getProperty("baseURL") + getPropertyValue("noDepositBonuses")) != 404) {
            if(!landingPage.getNoDepositTitleText().contains(expectedContent)){
                Assert.fail("The URL browsed might not be a correct one");
            }
        } else {
            Assert.fail("Subdirectory not found! Check subdirectory provided in the properties file.");
        }
    }




}
